package no.uib.inf101.gridview;

import no.uib.inf101.gridview.CellPositionToPixelConverter;
import no.uib.inf101.colorgrid.CellColor;
import no.uib.inf101.colorgrid.CellColorCollection;
import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.IColorGrid;

import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.Rectangle;
import java.awt.Shape;;

public class GridView extends JPanel {

  IColorGrid grid;
  private static final double OUTERMARGIN = 30;
  private static final Color MARGINCOLOR = Color.LIGHT_GRAY;

  public GridView(IColorGrid grid) {
    this.setPreferredSize(new Dimension(400, 300));
    this.grid = grid;
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    drawGrid(g2);
  }

  private void drawGrid(Graphics2D g2) {

    double width = this.getWidth() - 2 * OUTERMARGIN;
    double height = this.getHeight() - 2 * OUTERMARGIN;
    Shape rectanlge = new Rectangle.Double(OUTERMARGIN, OUTERMARGIN, width, height);
    g2.setColor(MARGINCOLOR);
    g2.fill(rectanlge);

    CellPositionToPixelConverter pixel = new CellPositionToPixelConverter(
        new Rectangle.Double(OUTERMARGIN, OUTERMARGIN, width, height), this.grid, OUTERMARGIN);

    drawCells(g2, grid, pixel);
  }

  private static void drawCells(Graphics2D g2, CellColorCollection grid2, CellPositionToPixelConverter pixel) {
    for (CellColor cell : grid2.getCells()) {
      if (cell.color() == null) {
        g2.setColor(Color.DARK_GRAY);
        g2.fill(pixel.getBoundsForCell(new CellPosition(cell.cellPosition().row(), cell.cellPosition().col())));
      } else {
        g2.setColor(cell.color());
        g2.fill(pixel.getBoundsForCell(new CellPosition(cell.cellPosition().row(), cell.cellPosition().col())));
      }
    }

  }

}
