package no.uib.inf101.gridview;

import java.awt.geom.Rectangle2D;

import no.uib.inf101.colorgrid.CellPosition;
import no.uib.inf101.colorgrid.GridDimension;

public class CellPositionToPixelConverter {

  Rectangle2D box;
  GridDimension gd;
  double margin;

  public CellPositionToPixelConverter(Rectangle2D box, GridDimension gd, double margin) {
    this.box = box;
    this.gd = gd;
    this.margin = margin;
  }

  public Rectangle2D getBoundsForCell(CellPosition cp) {

    double cellHeigth = (box.getHeight() - (margin * (gd.rows() + 1))) / gd.rows();
    double cellWidth = (box.getWidth() - (margin * (gd.cols() + 1))) / gd.cols();

    double cellY = (box.getY() + (this.margin * (cp.row() + 1)) + (cellHeigth * cp.row()));
    double cellX = (box.getX() + (this.margin * (cp.col() + 1)) + (cellWidth * cp.col()));

    return new Rectangle2D.Double(cellX, cellY, cellWidth, cellHeigth);

  }
}
