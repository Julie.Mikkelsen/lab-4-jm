package no.uib.inf101.colorgrid;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class ColorGrid implements IColorGrid {

  private int rows;
  private int cols;
  private ArrayList<CellColor> grid;

  public ColorGrid(int rows, int cols) {
    this.rows = rows;
    this.cols = cols;
    this.grid = new ArrayList<CellColor>();
    for (int r = 0; r < rows; r++) {
      for (int c = 0; c < cols; c++) {
        this.grid.add(new CellColor(new CellPosition(r, c), null));
      }
    }

  }

  @Override
  public int rows() {
    return this.rows;
  }

  @Override
  public int cols() {
    return this.cols;
  }

  @Override
  public List<CellColor> getCells() {

    return this.grid;
  }

  @Override
  public Color get(CellPosition pos) {
    if (pos.row() >= rows || pos.row() < 0) {
      throw new IndexOutOfBoundsException("Index out of bounds");
    }
    if (pos.col() >= cols || pos.col() < 0) {
      throw new IndexOutOfBoundsException("Index out of bounds");
    }
    return this.grid.get(pos.row() * cols + pos.col()).color();
  }

  @Override
  public void set(CellPosition pos, Color color) {
    if (pos.row() >= rows || pos.row() < 0) {
      throw new IndexOutOfBoundsException("Index out of bounds");
    }
    if (pos.col() >= cols || pos.col() < 0) {
      throw new IndexOutOfBoundsException("Index out of bounds");
    }
    this.grid.set(pos.row() * cols + pos.col(), new CellColor(pos, color));

  }

}
